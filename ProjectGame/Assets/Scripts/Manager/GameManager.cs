﻿using System;
using Spaceship;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Button startButton;
        [SerializeField] private RectTransform dialog;
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship,enemySpaceship1,enemySpaceship2,enemySpaceship3;
        [SerializeField] private ScoreManager scoreManager;
        public event Action OnRestarted;
        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceshipMoveSpeed;
        


        private void Awake()
        {
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(dialog != null, "dialog cannot be null");
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(scoreManager != null, "scoreManager cannot be null");
            Debug.Assert(playerSpaceshipHp > 0, "playerSpaceship hp has to be more than zero");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than zero");
            Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp has to be more than zero");
            Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than zero");
            startButton.onClick.AddListener(OnStartButtonClicked);
        }

        
        

        private void OnStartButtonClicked()
        {
            dialog.gameObject.SetActive(false);
            StartGame();
        }

        private void StartGame()
        {
            scoreManager.Init(this);
            SpawnPlayerSpaceship();
            if (ScoreCount.scoreValue == 30)
            {
                SpawnBossEnemySpaceship();
                return;
            }
            SpawnEnemySpaceship();
        }
        
        private void SpawnPlayerSpaceship()
        {
            var spaceship = Instantiate(playerSpaceship);
            spaceship.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spaceship.OnExploded += OnPlayerSpaceshipExploded;
        }

        private void OnPlayerSpaceshipExploded()
        {
            Restart();
        }

        private void SpawnEnemySpaceship()
        {
            var spaceship1 = Instantiate(enemySpaceship1);
            var spaceship2 = Instantiate(enemySpaceship2);
            var spaceship3 = Instantiate(enemySpaceship3);
            spaceship1.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
            spaceship2.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
            spaceship3.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
            spaceship1.OnExploded += OnEnemySpaceshipExploded;
            spaceship2.OnExploded += OnEnemySpaceshipExploded;
            spaceship3.OnExploded += OnEnemySpaceshipExploded;
        }

        private void SpawnBossEnemySpaceship()
        {
            var spaceship = Instantiate(enemySpaceship); 
            spaceship.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
            ScoreCount.scoreValue += 40;
            spaceship.OnExploded += OnEnemySpaceshipExploded;
        }

        public void OnEnemySpaceshipExploded()
        {
            ScoreCount.scoreValue += 10;
            if (ScoreCount.scoreValue == 30)
            {
                SceneManager.LoadScene(1);
            }
            else if (ScoreCount.scoreValue == 80)
            {
                Restart();
                ScoreCount.scoreValue = 0;
            }
        }
      
        
        private void Restart()
        {
            SceneManager.LoadScene(0);
            DestroyRemainingShips();
            dialog.gameObject.SetActive(true);
            OnRestarted?.Invoke();
        }
        
        private void DestroyRemainingShips()
        {
            var remainingEnimies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remainingEnimies)
            {
                Destroy(enemy);
            }
            
            var remainingPlayer = GameObject.FindGameObjectsWithTag("Player");
            foreach (var Player in remainingPlayer)
            {
                Destroy(Player);
                
            }
        }


    }
}
