﻿using TMPro;
using UnityEngine;


namespace Manager
{
    public class ScoreManager : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI finalScoreText;

        private GameManager gameManager;
        private int playerScore;
        
        public void Init(GameManager gameManager)
        {
            this.gameManager = gameManager;
            this.gameManager.OnRestarted += OnRestarted;
        }
        
        
        private void OnRestarted()
        {
            playerScore = ScoreCount.scoreValue;
            finalScoreText.text = $"Player Score : {playerScore}";
            gameManager.OnRestarted -= OnRestarted;
           
        }

        
    }
}


