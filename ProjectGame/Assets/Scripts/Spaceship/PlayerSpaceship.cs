using System;
using System.Net.Mime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Spaceship
{
    public class PlayerSpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;

        [SerializeField] private AudioClip playerExplosionSound;
        [SerializeField] private float playerExplosionSoundVolume = 0.5f;
        [SerializeField] private Text playerHPText;
        
        
        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
        }
        

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }
        

        public override void Fire()
        {
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init(Vector2.up);
        }
        

        public void TakeHit(int damage)
        {
            Hp -= damage;
            playerHPText.text = $"Player Hp : {Hp}";
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            AudioSource.PlayClipAtPoint(playerExplosionSound,Camera.main.transform.position,playerExplosionSoundVolume);
            Debug.Assert(Hp <= 0, "HP is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();
        }

        
    }
}